import sys
import time
from random import randrange, randint
import pygame

max_col = 11
COL = [None] * max_col
COL[0] = BLACK = (0, 0, 0)
COL[1] = GREEN = (0, 200, 0 )
COL[2] = RED = (200, 0, 0 )
COL[3] = WHITE = (255, 255, 255)
COL[4] = BLUE = (0, 0, 128)
COL[5] = SHADOW = (192, 192, 192)
COL[6] = LIGHTRED = (255, 100, 100)
COL[7] = LIGHTGREEN = (0, 255, 0 )
COL[8] = LIGHTBLUE = (0, 0, 255)
COL[9] = PURPLE = (102, 0, 102)
COL[10] = LIGHTPURPLE = (153, 0, 153)
OUT_OF_C = 8 # probability to meet a contact is 1 : OUT_OF_C
OUT_OF_I = 10 # probability to get infected is (5 - immunereignisse) : OUT_OF_I
IMMUN_TAGE = 120


class Person:
    def __init__(self, pos):
        self.pos = pos
        self.screen_pos = tuple([pos[i] * step[i] + offset[i] for i in range(2)])
        self.contacts = []
        self.krank_tage = -1
        self.immunereignisse = 0
        self.immun_tage = 0

    def init_relations(self, field):
        for neighbour in field.get_neighbours(self):
            self.contacts.append(neighbour)
        if randrange(0, 100) == 0:
            self.contacts.append(field.get_random(exclude = self))

    def process_self(self):
        if self.krank_tage >= 0:
            self.krank_tage += 1
        if self.krank_tage > 7:
            self.krank_tage = -1
            self.immunereignisse += 1
        if self.immunereignisse > 0:
            self.immun_tage +=1
            if self.immun_tage > IMMUN_TAGE:
                self.immunereignisse -= 1
                self.immun_tage = 0
        return (1 if self.is_krank() else 0, self.immunereignisse)

    def process_other(self):
        if self.is_krank():
            for contact in self.contacts:
                if randint(0, OUT_OF_C) == 0:
                    contact.virus_contact()
    
    def is_krank(self):
        return self.krank_tage >= 0

    def virus_contact(self):
        if randint(0, OUT_OF_I) < (5 - self.immunereignisse):
            if self.krank_tage == -1:
                self.krank_tage = 0

    def get_color(self):
        if self.krank_tage >= 0:
            return (255, 0, 0)
        else:
            if self.immunereignisse == 0:
                return (255, 255, 255)
            elif self.immunereignisse == 1:
                return (200, 255, 200)
            elif self.immunereignisse > 2:
                return (100, 255, 100)
            else:
                return (0, 255, 0)


class Field:
    def __init__(self, size):
        (self.width, self.height) = self.size = size
        self.matrix = [[Person((x, y)) for y in range(self.height)] for x in range(self.width)]
        self.all_persons = self.get_all_persons()
        self.num_persons = len(self.all_persons)
        for person in self.all_persons:
            person.init_relations(self)
        self.static_surface = self.draw_static_surface()

    #def set(self, xy, what):
    #    """ Field elemens are only set in constructor """
    #    self.matrix[xy[0]][xy[1]] = what

    def get(self, xy):
        """ get Person on Position xy """
        #print(f'c: {xy}')
        return self.matrix[xy[0]][xy[1]]

    def get_random(self, exclude = None):
        """ get random Person """
        (width, height) = self.size = size
        while True:
            r_pos = (randrange(0, width), randrange(0, height))
            if exclude is None:
                break
            if r_pos != exclude.pos:
                break
        return self.get(r_pos)

    def get_neighbours(self, person):
        """ get Neighbours """
        #print("get_neighbour")
        n_pos_list = []
        (x, y) = person.pos
        #print(f'a: {x}, {y}')
        #print(f'b: {self.width}, {self.height}')
        if x > 0:
            n_pos_list.append((x - 1, y))
        if x < (self.width - 1):
            n_pos_list.append((x + 1, y))
        if y > 0:
            n_pos_list.append((x, y - 1))
        if y < (self.height - 1):
            n_pos_list.append((x, y + 1))
        if x > 0 and y > 0:
            n_pos_list.append((x - 1, y - 1))
        if x < (self.width - 1) and y < (self.height - 1):
            n_pos_list.append((x + 1, y + 1))
        return [self.get(n_pos) for n_pos in n_pos_list]

    def get_all_persons(self):
        return [item for sublist in self.matrix for item in sublist]

    def random_person_virus_contact(self):
        self.all_persons[randint(0, self.num_persons)].virus_contact()

    def get_screen_pos(self, xy):
        """ get center of element xy on screen """
        #return [xy[i] * step[i] + offset[i] for i in range(2)]
        return self.get(xy).screen_pos

    #def get_env(self, xy):
    #    """ return list of neighbours as
    #        123
    #        456
    #        789 """
    #    x, y = xy
    #    sx, sy = size
    #    return ([self.get(((x + i) % sx, (y + j) % sy)) for i in range(-1, 2) for j in range (-1, 2)])

    def process(self):
        """ simulate actions of persons and infections """
        (num_krank, sum_immunereignisse) = (0, 0)
        for person in self.all_persons:
            (k, i) = person.process_self()
            num_krank += k
            sum_immunereignisse += i
        for person in self.all_persons:
            person.process_other()
        return (num_krank, sum_immunereignisse)

    def draw_static_surface(self):
        """ draw connections only once """
        surface = pygame.Surface(size_screen)
        for person in self.all_persons:
            for contact in person.contacts:
                pygame.draw.line(surface, COL[2], person.screen_pos, contact.screen_pos, 1)
        return surface

    def draw(self):
        """ draw current state """
        screen.blit(self.static_surface, (0, 0))
        for person in self.all_persons:
            pygame.draw.circle(screen, person.get_color(), person.screen_pos, radius)



pygame.init()
pygame.key.set_repeat(0, 0)
screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
size_screen = screen.get_size()
radius = 2

#size_screen = (1000, 500)
#screen = pygame.display.set_mode(size_screen)
#radius = 15

size = (150, 80)
step = [(size_screen[i] - 1) // size[i] for i in range (2)]
offset = [(step[i]) // 2 for i in range (2)]
num_col = 3
field = Field(size)
for i in range(10):
    field.random_person_virus_contact()


f = open("/tmp/virus_data.txt", "w")
i = 0

while 1:
    #screen.fill((0, 0, 0))
    field.draw()
    pygame.display.flip()
    #time.sleep(0.2)
    (num_krank, sum_immunereignisse) = field.process()
    f.write(",".join([str(i) for i in (i, num_krank, sum_immunereignisse)]) + '\n')
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                pygame.quit()
                f.close()
                sys.exit()
    i += 1

