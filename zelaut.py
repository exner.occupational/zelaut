import sys
import time
import random
import pygame

max_col = 11
COL = [None] * max_col
COL[0] = BLACK = (0, 0, 0)
COL[1] = GREEN = (0, 200, 0 )
COL[2] = RED = (200, 0, 0 )
COL[3] = WHITE = (255, 255, 255)
COL[4] = BLUE = (0, 0, 128)
COL[5] = SHADOW = (192, 192, 192)
COL[6] = LIGHTRED = (255, 100, 100)
COL[7] = LIGHTGREEN = (0, 255, 0 )
COL[8] = LIGHTBLUE = (0, 0, 255)
COL[9] = PURPLE = (102, 0, 102)
COL[10] = LIGHTPURPLE = (153, 0, 153)

pygame.init()

class Field:
    def __init__(self, size):
        width, height = self.size = size
        self.matrix = [[0 for _ in range(height)] for _ in range(width)]
    def set(self, xy, what):
        self.matrix[xy[0]][xy[1]] = what
    def get(self, xy):
        return self.matrix[xy[0]][xy[1]]
    def get_pos(self, xy):
        return [xy[i] * step[i] + offset[i] for i in range(2)]
    # return list of neighbours as
    # 123
    # 456
    # 789
    def get_env(self, xy):
        x, y = xy
        sx, sy = size
        return ([self.get(((x + i) % sx, (y + j) % sy)) for i in range(-1, 2) for j in range (-1, 2)])
    def init_random(self):
        width, height = self.size
        for y in range(height):
            for x in range(width):
                self.matrix[x][y] = random.randint(0, (num_col - 1))
    def init_random1(self):
        width, height = self.size
        for y in range(height):
            for x in range(width):
                if random.randint(0, 10) == 0:
                    self.matrix[x][y] = 1
    def init_random2(self):
        width, height = self.size
        for y in range(height):
            for x in range(width):
                if random.randint(0, 10) == 0:
                    self.matrix[x][y] = random.randint(0, (num_col - 1))
    def set_glider(self):
        self.matrix[4][4] = 1
        self.matrix[4][5] = 1
        self.matrix[5][5] = 1
        self.matrix[3][6] = 1
        self.matrix[5][6] = 1
    def process(self):
        width, height = self.size
        matrix = [[0 for _ in range(height)] for _ in range(width)]
        for y in range(height):
            for x in range(width):
                matrix[x][y] = waldbrand(self.get_env((x, y)))
                #matrix[x][y] = rule(self.get_env((x, y)))
                #matrix[x][y] = game_of_life(self.get_env((x, y)))
        self.matrix = matrix
    def draw(self):
        for y in range(self.size[1]):
            for x in range(self.size[0]):
                pygame.draw.circle(screen, COL[self.get((x, y))], self.get_pos((x, y)), radius)

screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN, 8)
size_screen = screen.get_size()
radius = 4

#size_screen = (1000, 500)
#screen = pygame.display.set_mode(size_screen)
#radius = 15

size = (200, 100)
step = [(size_screen[i] - 1) // size[i] for i in range (2)]
offset = [(step[i]) // 2 for i in range (2)]
num_col = 3
f = Field(size)
#f.init_random()
#f.set_glider()
f.init_random1()
#f.init_random2()

def neighbours_only(env):
    return [env[i] for i in [0, 1, 2, 3, 5, 6, 7, 8]]

# wenn 1 nur ein einziger 2 in der Umgebung, dann werde zu 2
def rule(env):
    n = neighbours_only(env)
    s = env[4]
    if s == 1 and (sum([1 if (i == 2) else 0 for i in n]) > (sum([1 if (i == 1) else 0 for i in n]))):
        return 2
    if s == 2 and (sum([1 if (i == 1) else 0 for i in n]) > (sum([1 if (i == 2) else 0 for i in n]))):
        return 0
    elif s == 0 and n[0] == 2:
        return 2
    elif s == 0 and n[2] == 1:
        return 1
    else:
        return 0

# 1. Eine „tote“ Zelle mit genau 3 lebendigen Nachbarn wird lebendig.
# 2. Eine „lebendige“ Zelle mit genau 2 oder genau 3 lebendigen Nachbarnbleibt am Leben.
# 3. In allen anderen Fällen stirbt die Zelle (an Vereinsamung oder Überbevölkerung)
def game_of_life(env):
    n = neighbours_only(env)
    s = env[4]
    if s == 0 and sum(n) == 3:
        return 1
    elif s == 1 and (sum(n) == 2 or sum(n) == 3):
        return 1
    else:
        return 0

# Waldbrand
# 3 Zustände: Baum, brennender Baum, Leerstelle
# Regel 1: Baum wird durch brennenden Nachbarbaum entzündet
# Regel 2: Bäume brennen eine Zeitperiode (Hier: 1)
# Regel 3: Zufall: Blitzschlag in Baum, Neuwuchs an Leerstelle
def waldbrand(env):
    n = neighbours_only(env)
    s = env[4]
    if s == 1 and sum([1 if (i == 2) else 0 for i in n]) > 0:
        return 2
    elif s == 2:
        return 0
    elif random.randint(0, 50) == 0:
        return 1
    elif random.randint(0, 5000000) == 0:
        return 2
    else:
        return s

pygame.key.set_repeat(0, 0)

while 1:
    screen.fill((0, 0, 0))
    f.draw()
    pygame.display.flip()
    #time.sleep(0.2)
    f.process()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                pygame.quit()
                sys.exit()

