import csv
from dash import Dash, html, dcc
import plotly.graph_objs as go
#import plotly.express as px # needs pandas

app = Dash(__name__)

x1 = []
y1 = []
x2 = []
y2 = []

with open('/tmp/virus_data.txt', 'r') as fd:
    reader = csv.reader(fd)
    for row in reader:
        x1.append(int(row[0]))
        y1.append(int(row[1]))
        x2.append(int(row[0]))
        y2.append(int(row[2]))

#fig = go.Figure(data=[go.Scatter(x=[1, 2, 3], y=[4, 1, 2])])
#fig = go.Figure(data=[go.Scatter(x=x1, y=y1), go.Scatter(x=x2, y=y2)])
fig = go.Figure(data=[go.Scatter(x=x1, y=y1, name="Infizierte"), go.Scatter(x=x2, y=y2, name="Immunereignisse")])
#fig = go.Figure(data=[go.Scatter(x=x1, y=y1)])

app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),

    html.Div(children='''
        Dash: A web application framework for your data.
    '''),

    dcc.Graph(
        id='example-graph',
        figure=fig
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)
